//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Thomas Harvey on 03/11/2016.
//
//

#include "SinOscillator.hpp"


SinOscillator::SinOscillator()
{
    
    frequency = 440.f;
    phasePosition = 0.f;
    sampleRate = 44000; //device->getCurrentSampleRate();
    
}

SinOscillator::~SinOscillator()
{
    
    
    
}


float SinOscillator::getSample()
{
    
    
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency)/sampleRate;
    float sinOut;
    
    phasePosition+= phaseIncrement;
    
    if(phasePosition > twoPi)
    {
        
        phasePosition = (phasePosition - twoPi);
    }
    
    sinOut = sin(phasePosition);
    
    sinOut *= amplitude;
    
    return sinOut;

}
void SinOscillator::setFrequency(float frequency_)
{
    
    frequency = frequency_;

}
void SinOscillator::setAmplitude(float amplitude_)
{
    
    amplitude = amplitude_;
    
}
