//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Thomas Harvey on 03/11/2016.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include <math.h>

#include "../../JuceLibraryCode/JuceHeader.h"

class SinOscillator
{
    
public:
    
    
    SinOscillator();
    ~SinOscillator();
    float getSample();
    void setFrequency(float frequency_);
    void setAmplitude(float amplitude_);
    
private:
    
    float amplitude = 0.5;
    float frequency;
    
    float phasePosition;
    float sampleRate;
    
    AudioIODevice* device;
    
   
    
};

#endif /* SinOscillator_hpp */
