/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    fAmplitude = 0;
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    
    frequency = message.getMidiNoteInHertz(message.getNoteNumber());
    
    //All MIDI inputs arrive here

}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    

    
    while(numSamples--)
    {
 

        *outL = sinOscillator.getSample();
        *outR = sinOscillator.getSample();
 
        inL++;
        inR++;
        outL++;
        outR++;
        
    }
}

void Audio::setAmplitude(float amplitude)

{
    fAmplitude = amplitude;
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
    frequency = 440.f;
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();

}

void Audio::audioDeviceStopped()
{

}